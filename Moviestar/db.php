<?php

$host = 'localhost';
$user = 'root';
$pass = '';
$db = 'movies_php';

$conn = new PDO("mysql:dbname=".$db.";host=".$host, $user, $pass);

// HABILITAR ERROS PDO
$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
$conn->setAttribute(PDO::ATTR_EMULATE_PREPARES, false);


?>
